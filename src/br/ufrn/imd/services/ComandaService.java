package br.ufrn.imd.services;

import java.util.List;

import br.ufrn.imd.dao.ComandaDao;
import br.ufrn.imd.dominio.Cliente;
import br.ufrn.imd.dominio.Comanda;
import br.ufrn.imd.dominio.Pedido;
import br.ufrn.imd.exceptions.NegocioException;

/**
 * Service de Comanda
 * @author sergio.luna
 *
 */
public class ComandaService {
	/**
	 * Dao de comanda para realizar as consultas/persist�ncia de dados
	 */
	private ComandaDao comandaDao;
	
	private EstatisticaFacade estatistiscaFacade;
	
	/**
	 * Construtor padr�o
	 */
	public ComandaService(){
		comandaDao = new ComandaDao();
		estatistiscaFacade = new EstatisticaFacade();
	}
	
	public Comanda buscarComanda(int id){
		return comandaDao.buscarComandaPorId(id);
		
	}
	
	/**
	 * Salvar comanda
	 */
	public void salvar(Comanda comanda) throws NegocioException{
		
		Comanda comandaAuxiliar = comanda;
		
		//buscar uma comanda com o mesmo id da comanda que ser� salva		
		Comanda comandaNoBanco = comandaDao.buscarComandaPorId(comandaAuxiliar.getId());
		if(comandaNoBanco != null && !comandaNoBanco.getCliente().equals(comanda.getCliente())) {
			throw new NegocioException("J� existe uma comanda com esse Identificador.");
		} else {
			comandaDao.salvar(comanda);
		}
	}
	
	/**
	 * M�todo para buscar a lista de comandas
	 */
	public List<Comanda> buscarListaComandas(){
		return comandaDao.buscarListaComandas();
	}
	
	/**
	 * Remover comanda da lista de comanda
	 */
	public void removerComanda(Comanda comanda){
		comandaDao.removerComanda(comanda);
	}
	
	/**
	 * Finalizar a comanda, setando o seu m�todo de pagamento e atualizando o seu status para finalizada
	 * @param comanda
	 */
	public void finalizarComanda(Comanda comanda) {
		Comanda comandaParaFinalizar = this.comandaDao.buscarComandaPorId(comanda.getId());
		if(comandaParaFinalizar != null && comandaParaFinalizar.isFinalizada() == false){
			comandaParaFinalizar.setMetodoPagamento(comanda.getMetodoPagamento());
			comandaParaFinalizar.setFinalizada(true);
			
			this.comandaDao.finalizarComanda(comandaParaFinalizar);
		}
	}

	/**
	 * Valida se o total da comanda excedeu o valor de limite definido pelo cliente
	 * @param totalComanda
	 */
	public void validarLimiteConsumo(Cliente cliente, Double totalComanda) throws NegocioException{
		//s� pode adicionar se o total da comanda atualizada n�o for maior que o limite de consumo definido pelo cliente
		//ou se o limite do usu�rio for definido como 0 -- sem limite
		if(!(cliente.getLimiteDeConsumo().compareTo(new Double(0.0)) == 0 || cliente.getLimiteDeConsumo().compareTo(totalComanda) > 0 )){
			throw new NegocioException("O total da comanda n�o pode exceder o limite do consumo definido pelo Cliente.");
		}
	}
	
	/**
	 * Metodo provis�rio para sincronizar os dados gerados randomicamente 
	 * para popular o banco.
	 * Utilizado na classe BancoDados
	 * @param comanda
	 */
	public static void atualizaTotal(Comanda comanda){
		for (Pedido p : comanda.getListPedidos()){
			p.setValor((double) p.getProduto().getValorProduto() * p.getQuantidade());
			p.getProduto().setQuantidadeNoEstoque(p.getProduto().getQuantidadeNoEstoque() - p.getQuantidade());
			comanda.setTotal(comanda.getTotal() + p.getValor());
			
		}
	}
	
	/**
	 * M�dia de clientes por dia 
	 * @return
	 */
	public int mediaClientesPorDia(){
		return estatistiscaFacade.clientesPorDia();
	}
	
	/**
	 * M�dia de pedidos por Cliente
	 * @return
	 */
	public int mediaPedidosPorCliente(){
		return estatistiscaFacade.pedidosPorCliente();
	}
	
	/**
	 * M�dia de valor de consumo por Cliente
	 * @return
	 */
	public Double mediaConsumoPorCliente(){
		return estatistiscaFacade.consumoPorCliente();
	}
}
