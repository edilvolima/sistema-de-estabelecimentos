package br.ufrn.imd.services;


import java.util.List;

import br.ufrn.imd.dao.ProdutoDao;
import br.ufrn.imd.dominio.Produto;
/**
 * Service de Produto
 * @author sergio.luna
 * 
 */
public class ProdutoService {
	
	/**
	 * Dao de produto para realizar as consultas/persist�ncia de dados
	 */
	private ProdutoDao produtoDao;
	
	/**
	 * Construtor padr�o
	 */
	public ProdutoService(){
		produtoDao = new ProdutoDao();
	}
	
	/**
	 * Salvar produto
	 */
	public void salvar(Produto produto){
		produtoDao.salvar(produto);
	}
	
	/**
	 * M�todo para buscar a lista de produtos
	 */
	public List<Produto> buscarListaProdutos(){
		return produtoDao.buscarListaProdutos();
	}
	
	/**
	 * Remover produto da lista de produtos
	 */
	public void removerProduto(Produto produto){
		produtoDao.removerProduto(produto);
	}
	
}
