package br.ufrn.imd.services;

import java.util.List;

import br.ufrn.imd.dao.AvaliacaoServicoDao;
import br.ufrn.imd.dominio.AvaliacaoServico;

/**
 * 
 * Service da avaliacao de servico
 * 
 * @author pdr_m
 *
 */
public class AvaliacaoServicoService {
	
	private AvaliacaoServicoDao avaliacaoServicoDao;
	
	/**
	 * Construtor da classe
	 */
	public AvaliacaoServicoService(){
		super();
		this.avaliacaoServicoDao = new AvaliacaoServicoDao();
	}
	
	/**
	 * Redirecionar a avaliacao para o respectivo Dao
	 * @param avaliacao
	 */
	public void salvarAvaliacao(AvaliacaoServico avaliacao) {
		this.avaliacaoServicoDao.salvarAvaliacao(avaliacao);
	}

	/**
	 * Retorna todas a avaliacoes de servico no banco de dados
	 * @return
	 */
	public List<AvaliacaoServico> getAvaliacoesDeServico() {
		return this.avaliacaoServicoDao.getAvaliacoesDeServico();
	}

}
