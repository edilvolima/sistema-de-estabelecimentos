package br.ufrn.imd.dominio;

import java.util.List;

public class Estabelecimento {

	private String nome;
	private String local;
	private Estoque estoque;
	private List<Comanda> listComandas;

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the local
	 */
	public String getLocal() {
		return local;
	}

	/**
	 * @param local
	 *            the local to set
	 */
	public void setLocal(String local) {
		this.local = local;
	}

	/**
	 * @return the estoque
	 */
	public Estoque getEstoque() {
		return estoque;
	}

	/**
	 * @param estoque
	 *            the estoque to set
	 */
	public void setEstoque(Estoque estoque) {
		this.estoque = estoque;
	}

	/**
	 * @return the listComandas
	 */
	public List<Comanda> getListComandas() {
		return listComandas;
	}

	/**
	 * @param listComandas
	 *            the listComandas to set
	 */
	public void setListComandas(List<Comanda> listComandas) {
		this.listComandas = listComandas;
	}

}
