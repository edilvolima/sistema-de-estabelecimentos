package br.ufrn.imd.dominio;

import java.util.List;

public class Estoque {

	private int totalProdutos;
	private List<Produto> listProdutos;

	/**
	 * @return the totalProdutos
	 */
	public int getTotalProdutos() {
		return totalProdutos;
	}

	/**
	 * @param totalProdutos
	 *            the totalProdutos to set
	 */
	public void setTotalProdutos(int totalProdutos) {
		this.totalProdutos = totalProdutos;
	}

	/**
	 * @return the listProdutos
	 */
	public List<Produto> getListProdutos() {
		return listProdutos;
	}

	/**
	 * @param listProdutos
	 *            the listProdutos to set
	 */
	public void setListProdutos(List<Produto> listProdutos) {
		this.listProdutos = listProdutos;
	}

}
