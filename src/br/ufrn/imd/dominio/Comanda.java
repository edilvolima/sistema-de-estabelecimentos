package br.ufrn.imd.dominio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
/**
 * Classe dominio de Comanda
 * @author sergio.luna
 *
 */
public class Comanda {
	
	/**
	 * Id da Comanda
	 */
	private int id;
	
	/**
	 * Cliente da comanda
	 */
	private Cliente cliente;
	
	/**
	 * Lista de pedidos
	 */
	private List<Pedido> listPedidos;
	
	/**
	 * Total do valor da comanda
	 */
	private Double total;
	
	/**
	 * Metodo do Pagamento utilizado do cliente
	 */
	private String metodoPagamento;
	
	/**
	 * Indica se a comanda foi finalizada ou n�o
	 */
	private boolean finalizada;

	private Date dataCriacao;
	
	/**
	 * Construtor padr�o da comanda
	 */
	public Comanda(){
		this.total = 0.0;
		this.finalizada = false;
		listPedidos = new ArrayList<Pedido>();
		Calendar c = Calendar.getInstance();
		this.dataCriacao = c.getTime();
	}
	
	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the listPedidos
	 */
	public List<Pedido> getListPedidos() {
		return listPedidos;
	}

	/**
	 * @param listPedidos
	 *            the listPedidos to set
	 */
	public void setListPedidos(List<Pedido> listPedidos) {
		this.listPedidos = listPedidos;
	}

	/**
	 * @return the total
	 */
	public Double getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(Double total) {
		this.total = total;
	}

	/**
	 * @return the metodoPagamento
	 */
	public String getMetodoPagamento() {
		return metodoPagamento;
	}

	/**
	 * @param metodoPagamento
	 *            the metodoPagamento to set
	 */
	public void setMetodoPagamento(String metodoPagamento) {
		this.metodoPagamento = metodoPagamento;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the dataCriacao
	 */
	public Date getDataCriacao() {
		return dataCriacao;
	}

	/**
	 * @param dataCriacao the dataCriacao to set
	 */
	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	/**
	 * @return if is finalizada
	 */
	public boolean isFinalizada() {
		return finalizada;
	}

	/**
	 * @param finalizada the finalizada to set
	 */
	public void setFinalizada(boolean finalizada) {
		this.finalizada = finalizada;
	}
	
	/**
	 * Clone de comanda
	 * @param comanda
	 */
	public void clone(Comanda comanda){
		this.setCliente(comanda.getCliente());
		this.setFinalizada(comanda.isFinalizada());
		this.setId(comanda.getId());
		this.setListPedidos(comanda.getListPedidos());
		this.setMetodoPagamento(comanda.getMetodoPagamento());
		this.setTotal(comanda.getTotal());
	}

	
}
