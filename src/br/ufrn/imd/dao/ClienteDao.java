package br.ufrn.imd.dao;

import java.util.List;

import br.ufrn.imd.dominio.Cliente;

/**
 * Dao de Cliente
 * @author sergio.luna
 * 
 */
public class ClienteDao {
	
	/**
	 * Construtor padr�o
	 */
	public ClienteDao(){
		
	}
	
	/**
	 * Salvar cliente
	 */
	public void salvar(Cliente cliente){
		BancoDados.getInstance().getClientes().add(cliente);
	}
	
	/**
	 * Busca lista de clientes
	 * @return
	 */
	public List<Cliente> buscarListaClientes(){
		return BancoDados.getInstance().getClientes();
	}
	
	/**
	 * Remover cliente da lista de clientes
	 */
	public void removerCliente(Cliente cliente){
		BancoDados.getInstance().getClientes().remove(cliente);
	}

	/**
	 * Buscar um cliente pelo seu login
	 * @param login
	 * @return
	 */
	public Cliente buscarClientePeloLogin(String login) {
		List<Cliente> listaDeClientes = BancoDados.getInstance().getClientes();
		
		for(Cliente cliente : listaDeClientes){
			if(cliente.getLogin().equals(login))
				return cliente;
		}
		
		return null;
	}

	/**
	 * Atualizar o cliente no banco de dados ap�s o caso de uso de limitar a comanda
	 * @param clienteNoBanco
	 */
	public void limitarConsumoDoCliente(Cliente clienteNoBanco) {
		BancoDados.getInstance().getClientes().set(
				BancoDados.getInstance().getClientes().indexOf(clienteNoBanco),
				clienteNoBanco);
		
		//Atualizar tamb�m a lista de usu�rios no banco -- isso se faz necess�rio devido � m�s decis�es durante o desenvolvimento -- refatora��es j� est�o em andamento
	}

}
