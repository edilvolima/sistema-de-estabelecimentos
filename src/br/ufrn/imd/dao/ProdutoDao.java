package br.ufrn.imd.dao;


import java.util.List;

import br.ufrn.imd.dominio.Produto;

/**
 * Dao de Produto
 * @author sergio.luna
 * 
 */
public class ProdutoDao {	
	
	/**
	 * Construtor padr�o
	 */
	public ProdutoDao(){
	
	}
	
	/**
	 * Salvar produto
	 */
	public void salvar(Produto produto){
		if (BancoDados.getInstance().getProdutos().isEmpty()){
			produto.setId(1);
		} else{
			produto.setId((BancoDados.getInstance().getProdutos().size()) + 1);
		}
		BancoDados.getInstance().getProdutos().add(produto);
	}
	
	/**
	 * Busca lista de produtos
	 * @return
	 */
	public List<Produto> buscarListaProdutos(){
		return BancoDados.getInstance().getProdutos();
	}
	
	/**
	 * Busca produto por Id
	 * @return
	 */
	public Produto buscarProdutoId(int id){
		return BancoDados.getInstance().getProdutos().get(id);
	}
	
	/**
	 * Busca produto por Descricao
	 * @return
	 */
	public Produto buscarProdutoDescricao(String descricao){
		List<Produto> produtos = BancoDados.getInstance().getProdutos();
			 for (Produto produto : produtos){
				 if(produto.getDescricao().equals(descricao)){
					 return produto;
				 }
			 }
		return null;
	}
	
	/**
	 * Remover produto da lista de produtos
	 */
	public void removerProduto(Produto produto){
		BancoDados.getInstance().getProdutos().remove(produto);
	}
	
}
