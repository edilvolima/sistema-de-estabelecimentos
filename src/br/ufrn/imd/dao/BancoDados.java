package br.ufrn.imd.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import br.ufrn.imd.dominio.Administrador;
import br.ufrn.imd.dominio.AvaliacaoServico;
import br.ufrn.imd.dominio.Cliente;
import br.ufrn.imd.dominio.Comanda;
import br.ufrn.imd.dominio.Gestor;
import br.ufrn.imd.dominio.Pedido;
import br.ufrn.imd.dominio.Produto;
import br.ufrn.imd.dominio.Usuario;
import br.ufrn.imd.services.ComandaService;

/**
 * 
 * @author Edilvo Lima
 *
 * Classe como Singleton
 * Simula um banco de dado em memoria
 * 
 */
public class BancoDados {
	/**
	 * Singleton
	 */
	private static BancoDados bancoDados;
	
	/**
	 * Lista de Produtos
	 */
	private List<Produto> produtos;
	
	/**
	 * Lista de Clientes
	 */
	private List<Cliente> clientes;
	
	/**
	 * Lista de Comandas
	 */
	private List<Comanda> comandas;
	
	/**
	 * Lista de Pedidos
	 */
	private List<Pedido> pedidos;
	
	/**
	 * Lista de usuarios
	 */
	private List<Usuario> usuarios;
	
	/**
	 * Lista das avaliacoes de servico
	 */
	private List<AvaliacaoServico> avaliacoesDeServico;
	
	/**
	 * Construtor padr�o
	 */
	private BancoDados(){
		produtos = new ArrayList<Produto>();
		clientes = new ArrayList<Cliente>();
		comandas = Collections.synchronizedList(new ArrayList<Comanda>());
		pedidos = new ArrayList<Pedido>();
		usuarios = new ArrayList<Usuario>();
		avaliacoesDeServico = new ArrayList<>();
		popularBancoDados();
	}
	
	/**
	 * 	Popular o banco de dados da aplica��o
	 */
	public void popularBancoDados(){
		
		//Usuarios
		
		Usuario clienteTeste = new Cliente("cliente");
		clienteTeste.setLogin("cliente");
		clienteTeste.setSenha("cliente");
		this.clientes.add((Cliente) clienteTeste);
		
		Usuario gestorTeste = new Gestor();
		gestorTeste.setLogin("gestor");
		gestorTeste.setSenha("gestor");
		
		Usuario admin = new Administrador();
		admin.setLogin("admin");
		admin.setSenha("secret");
		
		this.usuarios.add(clienteTeste);
		this.usuarios.add(gestorTeste);
		this.usuarios.add(admin);
		
		
		//Populando o banco inicial
		
		
		//Cliente 1
		Cliente cliente = new Cliente("Francisco");
		cliente.setLogin("francisco");
		cliente.setSenha("francisco");
		this.usuarios.add(cliente);
		this.clientes.add((Cliente) cliente);
		
		//Cliente 2
		Cliente cliente2 = new Cliente("Sergio");
		cliente2.setLogin("sergio");
		cliente2.setSenha("sergio");
		this.usuarios.add(cliente2);
		this.clientes.add((Cliente) cliente2);
		
		
		//Cliente 3
		Cliente cliente3 = new Cliente("Pedro");
		cliente3.setLogin("pedro");
		cliente3.setSenha("pedro");
		this.usuarios.add(cliente3);
		this.clientes.add((Cliente) cliente3);
		
		//Cliente 4
		Cliente cliente4 = new Cliente("Edilvo");
		cliente4.setLogin("edilvo");
		cliente4.setSenha("edilvo");
		this.usuarios.add(cliente4);
		this.clientes.add((Cliente) cliente4);
		
		//Cliente 5
		Cliente cliente5 = new Cliente("Uira");
		cliente5.setLogin("uira");
		cliente5.setSenha("uira");
		this.usuarios.add(cliente5);
		this.clientes.add((Cliente) cliente5);
		
		//Produto 1
		Produto produto = new Produto();
		produto.setDescricao("Coca-Cola");
		produto.setId(0);
		produto.setQuantidadeNoEstoque(100);
		produto.setTipo("Bebida");
//		produto.setValorProduto(3.50f);
		produto.setValorProduto(new Double(3.50));
		this.produtos.add(produto);
		
		//Produto 2
		Produto produto2 = new Produto();
		produto2.setDescricao("Espetinho");
		produto2.setId(1);
		produto2.setQuantidadeNoEstoque(150);
		produto2.setTipo("Comida");
//		produto2.setValorProduto(4.90f);
		produto2.setValorProduto(new Double(4.90));
		this.produtos.add(produto2);
		
		//Produto 3
		Produto produto3 = new Produto();
		produto3.setDescricao("Batata Frita");
		produto3.setId(2);
		produto3.setQuantidadeNoEstoque(200);
		produto3.setTipo("Comida");
//		produto3.setValorProduto(12.35f);
		produto3.setValorProduto(new Double(12.35));
		this.produtos.add(produto3);
		
		//Produto 4
		Produto produto4 = new Produto();
		produto4.setDescricao("Pa�oca");
		produto4.setId(3);
		produto4.setQuantidadeNoEstoque(125);
		produto4.setTipo("Comida");
//		produto4.setValorProduto(15);
		produto4.setValorProduto(new Double(15));
		this.produtos.add(produto4);
		

		//Produto 5
		Produto produto5 = new Produto();
		produto5.setDescricao("�gua");
		produto5.setId(4);
		produto5.setQuantidadeNoEstoque(300);
		produto5.setTipo("Bebida");
//		produto5.setValorProduto(1.25f);
		produto5.setValorProduto(new Double(1.25));
		this.produtos.add(produto5);
		
		//Produto 6
		Produto produto6 = new Produto();
		produto6.setDescricao("Guaran�");
		produto6.setId(5);
		produto6.setQuantidadeNoEstoque(150);
		produto6.setTipo("Bebida");
//		produto6.setValorProduto(3);
		produto6.setValorProduto(new Double(3));
		this.produtos.add(produto6);
		
		//Produto 7
		Produto produto7 = new Produto();
		produto7.setDescricao("Soda");
		produto7.setId(6);
		produto7.setQuantidadeNoEstoque(128);
		produto7.setTipo("Bebida");
//		produto7.setValorProduto(2.50f);
		produto7.setValorProduto(new Double(2.50));
		this.produtos.add(produto7);
		
		//Produto 8
		Produto produto8 = new Produto();
		produto8.setDescricao("Sandu�che");
		produto8.setId(7);
		produto8.setQuantidadeNoEstoque(200);
		produto8.setTipo("Comida");
//		produto8.setValorProduto(5);
		produto8.setValorProduto(new Double(5));
		this.produtos.add(produto8);
		
		//Produto 9
		Produto produto9 = new Produto();
		produto9.setDescricao("Cachorro Quente");
		produto9.setId(8);
		produto9.setQuantidadeNoEstoque(500);
		produto9.setTipo("Comida");
//		produto9.setValorProduto(3.50f);
		produto9.setValorProduto(new Double(3.50f));
		this.produtos.add(produto9);
		
		//Produto 10
		Produto produto10 = new Produto();
		produto10.setDescricao("Fil� com Fritas");
		produto10.setId(9);
		produto10.setQuantidadeNoEstoque(30);
		produto10.setTipo("Comida");
//		produto10.setValorProduto(30);
		produto10.setValorProduto(new Double(30));
		this.produtos.add(produto10);
		
		
		//1 Comanda para cada Cliente
		int index = 0;
		Comanda comanda;
		for (Cliente c : clientes){
			comanda = new Comanda();
			comanda.setCliente(c);
			comanda.setFinalizada(false);
			comanda.setId(index);
			
			//Radomizar data
			Random d = new Random();
			Calendar ca = Calendar.getInstance();
			ca.set(2017, d.nextInt(11), d.nextInt(25));
			Date dataCriacao = ca.getTime();
			comanda.setDataCriacao(dataCriacao);
			
			getComandas().add(comanda);
			index++;
		}
				
		//Pedido 1
		Random r = new Random();
		Pedido pedido;
		for(Comanda c : comandas){
			for(Produto p : produtos){
				pedido = new Pedido();
				pedido.setComanda(c);
				if(p.getId() > r.nextInt(produtos.size())){
					pedido.setProduto(p);
					pedido.setQuantidade(r.nextInt(2)+1);
					c.getListPedidos().add(pedido);
				}
			}
			ComandaService.atualizaTotal(c);
		}
		
		
//		//Comanda 1
//		Comanda comanda = new Comanda();
//		comanda.setCliente(cliente);
//		comanda.setFinalizada(false);
//		comanda.setId(0);
//		this.comandas.add(comanda);
//		
//		//Pedido 1
//		Pedido pedido = new Pedido();
//		pedido.setComanda(comanda);
//		pedido.setProduto(produto);
//		pedido.setQuantidade(1);
//		this.pedidos.add(pedido);
//		
//		
//		comanda.setListPedidos(this.pedidos);
		
		
//		Criar uma nova comanda para o cliente
//		Comanda comanda1 = new Comanda();
//		comanda1.setCliente(cliente);
//		comanda1.setFinalizada(false);
//		comanda1.setId(1);
//		
//		Adicionar a comanda no banco
//		this.comandas.add(comanda1);
//		
//		Criar um pedido e popul�-lo com produtos buscados do banco
//		Pedido pedido1 = new Pedido();
//		
//		Buscar pelo produto, definir a quantidade e adicion�-lo ao pedido
//		Produto produtoPedido1 = this.buscarPedidoPorNome("produto 1");
//		produtoPedido1.setQuantidade(3);
//		pedido1.addProduto(produtoPedido1);
//		
//		Produto produtoPedido2 = this.buscarPedidoPorNome("produto 2");
//		produtoPedido2.setQuantidade(1);
//		pedido1.addProduto(produtoPedido2);
//		
//		Adicionar o pedido � comanda
//		comanda1.addPedido(pedido1);
		
		
	}
	
	/**
	 * Unica instancia do Banco de Dados
	 * @return
	 */
	public static BancoDados getInstance() {
		if (bancoDados == null)
			bancoDados = new BancoDados();

		return bancoDados;
	}

	/**
	 * @return the produtos
	 */
	public List<Produto> getProdutos() {
		return produtos;
	}

	/**
	 * @param produtos the produtos to set
	 */
	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	/**
	 * @return the clientes
	 */
	public List<Cliente> getClientes() {
		return clientes;
	}

	/**
	 * @param clientes the clientes to set
	 */
	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	/**
	 * @return the comandas
	 */
	public List<Comanda> getComandas() {
		return comandas;
	}

	/**
	 * @param comandas the comandas to set
	 */
	public void setComandas(List<Comanda> comandas) {
		this.comandas = comandas;
	}

	/**
	 * @return the pedidos
	 */
	public List<Pedido> getPedidos() {
		return pedidos;
	}

	/**
	 * @param pedidos the pedidos to set
	 */
	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	/**
	 * Retorna a lista de usuarios no banco de dados
	 * @return
	 */
	public List<Usuario> getUsuarios() {
		return this.usuarios;
	}	
	
	/**
	 * Set a lista de usu�rios no banco de dados
	 */
	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	/**
	 * Retorna a lista de avaliacoes de servico
	 * @return
	 */
	public List<AvaliacaoServico> getAvaliacoesDeServico() {
		return this.avaliacoesDeServico;
	}
	
	/**
	 * Seta a lista de avaliacoes de servico
	 * @param avaliacoesDeServico
	 */
	public void setAvaliacoesDeServico(List<AvaliacaoServico> avaliacoesDeServico) {
		this.avaliacoesDeServico = avaliacoesDeServico;
	}
}
