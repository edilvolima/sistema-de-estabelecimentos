package br.ufrn.imd.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.ufrn.imd.dominio.Comanda;
import br.ufrn.imd.dominio.Pedido;
import br.ufrn.imd.exceptions.NegocioException;
import br.ufrn.imd.services.ComandaService;
import br.ufrn.imd.services.PedidoService;


/**
 * MBean de Pedido
 * @author S�rgio
 *
 */

@SessionScoped
@ManagedBean(name = "pedidoMBean")
public class PedidoMBean {
	
	/**
	 * Service de Pedido
	 */
	private PedidoService pedidoService;
	private ComandaService comandaService;
	
	/**
	 * Objeto do tipo Pedido
	 */
	private Pedido obj;
	
	/**
	 * Lista de Pedidos
	 */
	private List<Pedido> listaPedidos;
	
	/**
	 * Comanda do Pedido
	 */
	private Comanda comanda;
	
	/**
	 * M�todo construtor
	 */
	public PedidoMBean(){
		obj = new Pedido();
		pedidoService = new PedidoService();
		comandaService = new ComandaService();
		listaPedidos = new ArrayList<Pedido>();
	}
	
	/**
	 * Retornar ao diretorio padr�o da p�gina
	 * @return
	 */
	protected String getDir() {
		return "/pages/comanda";
	}
	
	/**
	 * Salvar os Pedidos na Comanda
	 */
	public String salvar(Comanda comanda){
		
		try {
			List<Pedido> ls = this.getListaPedidos();
			Comanda comandaAuxiliar = new Comanda(); 
			comandaAuxiliar.setTotal(comanda.getTotal());
			
			for (Pedido p : ls){
				p.setValor((double) p.getProduto().getValorProduto() * p.getQuantidade());
				comandaAuxiliar.setTotal(comandaAuxiliar.getTotal() + p.getValor());
			}
			
			comandaService.validarLimiteConsumo(comanda.getCliente(), comandaAuxiliar.getTotal());
			
			for (Pedido p : ls){
				p.setValor((double) p.getProduto().getValorProduto() * p.getQuantidade());
				p.getProduto().setQuantidadeNoEstoque(p.getProduto().getQuantidadeNoEstoque() - p.getQuantidade());
				p.setComanda(comanda);
				comanda.setTotal(comanda.getTotal() + p.getValor());
				comanda.getListPedidos().add(p);
			}
			
			comandaService.salvar(comanda);
		} catch (NegocioException e) {
			comanda.getListPedidos().removeAll(this.getListaPedidos());
			FacesMessage msg = new FacesMessage(e.getMessage());
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage("", msg);
			e.printStackTrace();
		}
		
		return getDir() + "/list.jsf";
	}
	
	/**
	 * Salvar os Pedidos na Comanda
	 */
	public String salvarPedidoCliente(Comanda comanda){
		
		try {
			
			List<Pedido> ls = this.getListaPedidos();
			Comanda comandaAuxiliar = new Comanda(); 
			comandaAuxiliar.setTotal(comanda.getTotal());
			
			for (Pedido p : ls){
				p.setValor((double) p.getProduto().getValorProduto() * p.getQuantidade());
				comandaAuxiliar.setTotal(comandaAuxiliar.getTotal() + p.getValor());
			}
			
			comandaService.validarLimiteConsumo(comanda.getCliente(), comandaAuxiliar.getTotal());
			
			for (Pedido p : ls){
				p.setValor((double) p.getProduto().getValorProduto() * p.getQuantidade());
				p.getProduto().setQuantidadeNoEstoque(p.getProduto().getQuantidadeNoEstoque() - p.getQuantidade());
				p.setComanda(comanda);
				comanda.setTotal(comanda.getTotal() + p.getValor());
				comanda.getListPedidos().add(p);
			}
			
			comandaService.salvar(comanda);
			
		} catch (NegocioException e) {
			FacesMessage msg = new FacesMessage(e.getMessage());
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage("", msg);
			e.printStackTrace();
		}
				
		return "/pages/cliente/comanda.jsf";
	}
	
	/**
	 * Cria��o da lista de pedidos que s�o acrescentados na Comanda, no m�todo salvar()
	 * @param comanda
	 * @return
	 */
	public String add(Comanda comanda){
		this.addPedido(obj);
		obj = new Pedido();
		return getDir() + "/formPedido.jsf";
	}
	
	/**
	 * Cria��o da lista de pedidos que s�o acrescentados na Comanda, no m�todo salvar()
	 * @param comanda
	 * @return
	 */
	public String addPedidoCliente(Comanda comanda){
		this.addPedido(obj);
		obj = new Pedido();
		
		return "/pages/cliente/formPedido.jsf";
	}

	/**
	 * @return the obj
	 */
	public Pedido getObj() {
		return obj;
	}

	/**
	 * @param obj the obj to set
	 */
	public void setObj(Pedido obj) {
		this.obj = obj;
	}
	
	/**
	 * Redirecionar para o formPedido
	 * @param comanda
	 * @return
	 */
	public String realizarPedido(Comanda comanda){
		obj = new Pedido();
		this.comanda = comanda;
		this.setListaPedidos(new ArrayList<Pedido>());
		return getDir() + "/formPedido.jsf";
	}
	
	/**
	 * Redirecionar para o formPedido do Cliente
	 * @param comanda
	 * @return
	 */
	public String realizarPedidoCliente(Comanda comanda){
		obj = new Pedido();
		this.comanda = comanda;
		this.setListaPedidos(new ArrayList<Pedido>());
		return "/pages/cliente/formPedido.jsf";
	}

	/**
	 * @return the listaPedidos
	 */
	public List<Pedido> getListaPedidos() {
		//listaPedidos = pedidoService.buscarListaPedidos();
		return listaPedidos;
	}

	/**
	 * @param listaPedidos the listaPedidos to set
	 */
	public void setListaPedidos(List<Pedido> listaPedidos) {
		this.listaPedidos = listaPedidos;
	}

	/**
	 * @return the comanda
	 */
	public Comanda getComanda() {
		return comanda;
	}

	/**
	 * @param comanda the comanda to set
	 */
	public void setComanda(Comanda comanda) {
		this.comanda = comanda;
	}
	
	public void addPedido(Pedido pedido){
		this.listaPedidos.add(pedido);
	}

}
