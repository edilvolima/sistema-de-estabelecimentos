package br.ufrn.imd.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.ufrn.imd.dominio.Produto;
import br.ufrn.imd.services.ProdutoService;

/**
 * MBean de Produto
 * 
 * @author sergio.luna
 *
 */
@SessionScoped
@ManagedBean(name = "produtoMBean")
public class ProdutoMBean {

	/**
	 * Service de Produto
	 */
	private ProdutoService produtoService;

	/**
	 * Objeto do tipo Produto
	 */
	private Produto obj;

	/**
	 * Lista de Produtos
	 */
	private List<Produto> listaProdutos;

	/**
	 * M�todo construtor
	 */
	public ProdutoMBean() {
		obj = new Produto();
		produtoService = new ProdutoService();
		listaProdutos = new ArrayList<Produto>();
	}

	/**
	 * Retornar ao diretorio padr�o da p�gina
	 * 
	 * @return
	 */
	protected String getDir() {
		return "/pages/produto";
	}

	/**
	 * Salvar um produto
	 */
	public String salvar() {
		produtoService.salvar(obj);
		return getDir() + "/list.jsf";
	}

	/**
	 * Editar atributos de um produto
	 */
	public String editar(Produto produto) {
		obj = produto;
		return getDir() + "/form.jsf";
	}

	/**
	 * Remover um produto
	 */
	public String remover(Produto produto) {
		produtoService.removerProduto(produto);
		return getDir() + "/list.jsf";
	}

	/**
	 * Visualizar um produto
	 */
	public String visualizar(Produto produto) {
		obj = produto;
		return getDir() + "/visualizar.jsf";
	}

	/**
	 * Redireciona para o form atualizar o objeto
	 */
	public String novo() {
		obj = new Produto();
		return getDir() + "/form.jsf";
	}

	/**
	 * Redireciona para o menu principal
	 */
	public String menu() {
		return "/index.jsf";
	}

	/**
	 * @return the listaProdutos
	 */
	public List<Produto> getListaProdutos() {
		listaProdutos = produtoService.buscarListaProdutos();
		return listaProdutos;
	}

	/**
	 * @param listaProdutos
	 *            the listaProdutos to set
	 */
	public void setListaProdutos(List<Produto> listaProdutos) {
		this.listaProdutos = listaProdutos;
	}

	/**
	 * @return the obj
	 */
	public Produto getObj() {
		return obj;
	}

	/**
	 * @param obj
	 *            the obj to set
	 */
	public void setObj(Produto obj) {
		this.obj = obj;
	}

}
