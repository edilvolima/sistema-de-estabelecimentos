package br.ufrn.imd.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.ufrn.imd.dao.BancoDados;
import br.ufrn.imd.dominio.Cliente;
import br.ufrn.imd.dominio.Comanda;
import br.ufrn.imd.services.ClienteService;
import br.ufrn.imd.services.ComandaService;

/**
 * MBean de Cliente
 * 
 * @author sergio.luna
 *
 */
@SessionScoped
@ManagedBean(name = "clienteMBean")
public class ClienteMBean {

	/**
	 * Service de Cliente
	 */
	private ClienteService clienteService;
	
	/**
	 * Service de Comanda
	 */
	private ComandaService comandaService;

	/**
	 * Objeto do tipo Cliente
	 */
	private Cliente obj;
	
	/**
	 * Lista de Comandas
	 */
	private List<Comanda> listaComandas;

	/**
	 * Lista de Clientes
	 */
	private List<Cliente> listaClientes;

	/**
	 * M�todo construtor
	 */
	public ClienteMBean() {
		obj = new Cliente();
		clienteService = new ClienteService();
		comandaService = new ComandaService();
		listaClientes = new ArrayList<Cliente>();
		listaComandas = new ArrayList<Comanda>();
	}

	/**
	 * Retornar ao diretorio padr�o da p�gina
	 * 
	 * @return
	 */
	protected String getDir() {
		return "/pages/cliente";
	}

	/**
	 * Salvar um cliente
	 */
	public String salvar() {
		clienteService.salvar(obj);
		return getDir() + "/list.jsf";
	}

	/**
	 * Editar atributos de um cliente
	 */
	public String editar(Cliente cliente) {
		obj = cliente;
		return getDir() + "/form.jsf";
	}

	/**
	 * Remover um cliente
	 */
	public String remover(Cliente cliente) {
		clienteService.removerCliente(cliente);
		return getDir() + "/list.jsf";
	}

	/**
	 * Visualizar um cliente
	 */
	public String visualizar(Cliente cliente) {
		obj = cliente;
		return getDir() + "/visualizar.jsf";
	}

	/**
	 * Redireciona para o form atualizar o objeto
	 */
	public String novo() {
		obj = new Cliente();
		return getDir() + "/form.jsf";
	}

	/**
	 * Redireciona para o menu principal
	 */
	public String menu() {
		return getDir() + "/conta.jsf";
	}
	

	/**
	 * Acompanhar a comanda do cliente logado
	 */
	public String acompanharComanda(String login) {
		obj.setLogin(login);
		return getDir() + "/comanda.jsf";
	}

	/**
	 * @return the listaComandas
	 */
	public List<Comanda> getListaComandas() {
		listaComandas = comandaService.buscarListaComandas();
		
		for (Iterator<Comanda> i = listaComandas.iterator(); i.hasNext();) {
			Comanda comanda = i.next();
			if(comanda.getCliente().getLogin() != obj.getLogin()){
				i.remove();
			}
		}
		
		return listaComandas;
	}

	/**
	 * @param listaComandas the listaComandas to set
	 */
	public void setListaComandas(List<Comanda> listaComandas) {
		this.listaComandas = listaComandas;
	}

	/**
	 * @return the listaClientes
	 */
	public List<Cliente> getListaClientes() {
		listaClientes = clienteService.buscarListaClientes();
		return listaClientes;
	}

	/**
	 * @param listaClientes
	 *            the listaClientes to set
	 */
	public void setListaClientes(List<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	/**
	 * @return the obj
	 */
	public Cliente getObj() {
		return obj;
	}

	/**
	 * @param obj
	 *            the obj to set
	 */
	public void setObj(Cliente obj) {
		this.obj = obj;
	}

	/**
	 * Redireciona para a p�gina de limitar o consumo
	 * 
	 * @return
	 */
	public String irParaLimitarConsumo(String login) {
		obj.setLogin(login);
		return this.getDir() + "/limitarConsumo.jsf";
	}
	
	/**
	 * Define o limite de consumo do cliente
	 * @return
	 */
	public String limitarConsumo() {
		
		this.clienteService.limitarConsumo(this.obj);
		
		return this.getDir() + "/conta.jsf";
	}
}
