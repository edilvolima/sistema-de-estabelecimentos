package br.ufrn.imd.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.ufrn.imd.dominio.Cliente;
import br.ufrn.imd.dominio.Comanda;
import br.ufrn.imd.dominio.Pedido;
import br.ufrn.imd.dominio.Produto;
import br.ufrn.imd.services.ComandaService;
import br.ufrn.imd.services.PedidoService;
import br.ufrn.imd.services.ProdutoService;
/**
 * MBean de Consumo
 * 
 * @author sergio.luna
 *
 */
@SessionScoped
@ManagedBean(name = "consumoMBean")
public class ConsumoMBean {
	
	/**
	 * Service de Comanda
	 */
	private ComandaService comandaService;
	
	/**
	 * Service de Produto
	 */
	private ProdutoService produtoService;
	
	/**
	 * Lista de Comandas
	 */
	private List<Comanda> listaComandas;
	
	/**
	 * Lista de Produtos
	 */
	private List<Produto> listaProdutos;
	
	/**
	 * Construtor Padr�o
	 */
	public ConsumoMBean(){
		comandaService = new ComandaService();
		produtoService = new ProdutoService();
		listaComandas = new ArrayList<Comanda>();
		listaProdutos = new ArrayList<Produto>();
	}
	
	/**
	 * Retornar ao diretorio padr�o da p�gina
	 * 
	 * @return
	 */
	protected String getDir() {
		return "/pages/consumo";
	}
	
	/**
	 * Redireciona para a pagina de menu de estatisticas do consumo
	 */
	public String novo() {
		return getDir() + "/menuEstatistica.jsf";
	}
	
	/**
	 * Redireciona para a pagina de resultados gerais das estatisticas de consumo
	 */
	public String resultadosGerais() {
		return getDir() + "/resultadosGerais.jsf";
	}
	
	/**
	 * Redireciona para a pagina de tabela de produtos das estatisticas de consumo
	 */
	public String tabelaProdutos() {
		return getDir() + "/tabelaProdutos.jsf";
	}
	
	/**
	 * Retornar a quantidade de comandas do estabelecimento
	 * @return
	 */
	public int totalComanda(){
		listaComandas = comandaService.buscarListaComandas();
		return listaComandas.size();
	}
	
	/**
	 * Retornar o total arrecado pelo estabelecimento
	 * @return
	 */
	public String totalArrecadado(){
		listaComandas = comandaService.buscarListaComandas();
		double totalArrecadado = 0.0;
		
		for(Comanda comanda: listaComandas){
			totalArrecadado += comanda.getTotal();
		}
		
		String resultado = String.format("%.2f", totalArrecadado);
		
		return resultado;
	}
	
	/**
	 * Retornar o total de produtos consumidos no estabelecimento
	 */
	public int totalProdutosConsumidos(){
		listaComandas = comandaService.buscarListaComandas();
		int totalProdutos = 0;
		
		for(Comanda comanda: listaComandas){
			for(Pedido pedido: comanda.getListPedidos()){
				totalProdutos += pedido.getQuantidade();
			}
			
		}
		
		return totalProdutos;
	}
	
	/**
	 * A m�dia de pedidos por comandas
	 * Soma da Quantidade de Pedidos de todas as comandas dividido
	 * pela quantidade total de comandas
	 */
	public String mediaPedidosComanda(){
		listaComandas = comandaService.buscarListaComandas();
		int totalPedidosComanda = 0;
		double media = 0.0;
		
		for(Comanda comanda: listaComandas){
			totalPedidosComanda += comanda.getListPedidos().size();	
		}
		
		media = ((float)totalPedidosComanda/(float)listaComandas.size());
		String resultado = String.format("%.2f", media);
		
		return resultado;
	}
	
	/**
	 * Retorna a quantidade em que o produto foi vendido no estabelecimento
	 * @return
	 */
	public int quantidadeProdutoVendido(Produto produto){
		listaComandas = comandaService.buscarListaComandas();
		List<Integer> listaProdutosVendidos = new ArrayList<Integer>();
		int quantidadeProdutoVendido = 0;
		
		for(Comanda comanda: listaComandas){
			List<Pedido> listaPedidos = new ArrayList<Pedido>();
			listaPedidos = comanda.getListPedidos();
			for(Pedido pedido: listaPedidos){
				if(pedido.getProduto().equals(produto)){
					listaProdutosVendidos.add(pedido.getQuantidade());				}
			}
		}
		
		for(Integer i: listaProdutosVendidos){
			quantidadeProdutoVendido += i;
		}
		
		return quantidadeProdutoVendido;
	}
	
	/**
	 * Retorna o valor arrecadado de um determinado produto do estabelecimento
	 * @return
	 */
	public String valorArrecadadoProduto(Produto produto){
		listaComandas = comandaService.buscarListaComandas();
		List<Double> listaProdutosVendidos = new ArrayList<Double>();
		double valorArrecadadoProduto = 0.0;
		
		for(Comanda comanda: listaComandas){
			List<Pedido> listaPedidos = new ArrayList<Pedido>();
			listaPedidos = comanda.getListPedidos();
			for(Pedido pedido: listaPedidos){
				if(pedido.getProduto().equals(produto)){
					listaProdutosVendidos.add(pedido.getValor());				}
			}
		}
		
		for(Double d: listaProdutosVendidos){
			valorArrecadadoProduto += d;
		}
		
		String resultado = String.format("%.2f", valorArrecadadoProduto);
		
		return resultado;
	}
	
	/**
	 * Retorna o total de clientes que consumiram aquele produto
	 * @return
	 */
	public int totalClienteConsumiram(Produto produto){
		listaComandas = comandaService.buscarListaComandas();
		List<Cliente> listaClientesConsumiram = new ArrayList<Cliente>();
		
		for(Comanda comanda: listaComandas){
			List<Pedido> listaPedidos = new ArrayList<Pedido>();
			listaPedidos = comanda.getListPedidos();
			for(Pedido pedido: listaPedidos){
				if(pedido.getProduto().equals(produto) && !listaClientesConsumiram.contains(comanda.getCliente())){
					listaClientesConsumiram.add(pedido.getComanda().getCliente());				}
			}
		}
		
		return listaClientesConsumiram.size();
	}

	/**
	 * @return the listaComandas
	 */
	public List<Comanda> getListaComandas() {
		return listaComandas;
	}

	/**
	 * @param listaComandas the listaComandas to set
	 */
	public void setListaComandas(List<Comanda> listaComandas) {
		this.listaComandas = listaComandas;
	}

	/**
	 * @return the listaProdutos
	 */
	public List<Produto> getListaProdutos() {
		listaProdutos = produtoService.buscarListaProdutos();
		return listaProdutos;
	}

	/**
	 * @param listaProdutos the listaProdutos to set
	 */
	public void setListaProdutos(List<Produto> listaProdutos) {
		this.listaProdutos = listaProdutos;
	}

}
