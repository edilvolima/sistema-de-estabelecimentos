package br.ufrn.imd.controllers;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.ufrn.imd.dominio.Administrador;
import br.ufrn.imd.dominio.Cliente;
import br.ufrn.imd.dominio.Gestor;
import br.ufrn.imd.dominio.Usuario;
import br.ufrn.imd.services.UsuarioService;

/**
 * 
 * MBean para login/logout
 * 
 * @author pdr_m
 *
 */
@SessionScoped
@ManagedBean(name = "loginMBean")
public class LoginMBean {
	/**
	 * Objeto Usuario do MBean
	 */
	private Usuario usuario;

	/**
	 * Objeto que guardar� o usu�rio logado no sistema
	 */
	private Usuario usuarioLogado;

	/**
	 * Service do usu�rio
	 */
	private UsuarioService usuarioService;

	/**
	 * Construtor do MBean
	 */
	public LoginMBean() {
		this.usuario = new Usuario();
		this.usuarioService = new UsuarioService();
		this.usuarioLogado = null;
	}

	/**
	 * Pega o objeto usu�rio da classe
	 * 
	 * @return usuario
	 */
	public Usuario getUsuario() {
		return usuario;
	}

	/**
	 * Define um novo objeto para o usu�rio da classe
	 * 
	 * @param usuario
	 */
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * Pega o objeto usuarioLogado
	 * 
	 * @return usuarioLogado
	 */
	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	/**
	 * Define um novo objeto para o usuarioLogado
	 * 
	 * @param usuarioLogado
	 */
	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	/**
	 * Realiza o login do usuario
	 * 
	 * @return o direcionamento depois de login
	 */
	public String logar() {
		String resultado = "";
		Usuario usuarioDoBanco = new Usuario();

		for (Usuario u : this.usuarioService.getListaUsuarios()) {
			if (u.getLogin().equals(usuario.getLogin())) {
				usuarioDoBanco = u;
			}
		}

		if (usuarioDoBanco.getLogin() != null) {
			if (usuarioDoBanco.getSenha().equals(usuario.getSenha())) {
				this.setUsuarioLogado(usuarioDoBanco);
				resultado = this.menuPrincipal();
			}
		} else {
			resultado = "Usu�rio/Senha incorreta.";
		}

		if (resultado.equals("Usu�rio/Senha incorreta.")) {
			FacesMessage msg = new FacesMessage(resultado);
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage("", msg);
		}

		return resultado;

	}

	/**
	 * Realiza o logout e redireciona o Usuario para a tela inicial.
	 * 
	 * @return
	 */
	public String deslogar() {
		this.setUsuarioLogado(null);
		return "/index.jsf";
	}

	/**
	 * Retorna o diretorio do menu principal de cada usuario
	 * 
	 * @return
	 */
	public String menuPrincipal() {
		String resultado = "";

		if (usuarioLogado instanceof Cliente) {
			resultado = "/pages/cliente/conta.jsf";
		} else if (usuarioLogado instanceof Gestor) {
			resultado = "/pages/gestor/conta.jsf";
		} else if (usuarioLogado instanceof Administrador) {
			resultado = "/pages/admin/conta.jsf";
		}

		return resultado;
	}

}
